from flask import Flask, request, render_template
from random import randint

app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = int(request.form['dividend'])
    divisor = int(request.form['divisor'])
    message = ""
    if request.method == "POST":
        if dividend%4 == 0:
            message = message+ "Dividend is a multiple of 4"
        elif dividend%2 == 0:
            message = message+"Dividend is even"
        else:
            message = message+"Dividend is odd"
        result = dividend/divisor
        message = message+f", Result is: {result}"
        if dividend%divisor == 0:
            message = message+", Dividend divides evenly by the divisor"
        else:
            message = message+", Dividend does NOT divide evenly by the divisor"

    return f"{message}"

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    grade = int(request.form['grade'])
    if grade > 100 or grade < 0:
        return "Incorrect score, please try again"
    if grade >= 95:
        return f"{name}, your grade is: A"
    if grade < 95 and grade >=80:
        return f"{name}, your grade is: B"
    if grade < 80 and grade >=70:
        return f"{name}, your grade is: C"
    if grade < 70 and grade >=60:
        return f"{name}, your grade is: D"
    if grade < 60:
        return f"{name}, your grade is: F"

    return ""

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    value = int(request.form['value'])
    if value<0:
        return"Invalid number, please try again"
    else:
        return f"The result is: {value*value}"
    return ""

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    number = int(request.form['number'])
    if number >100 or number <0:
        return "Incorrect number, please try again"
    match = False
    randGen = randint(1, 100)
    counter = 0
    while not match:
        if number == randGen:
            match = True
            return f"It took {counter} tries"
        else:
            counter +=1
            randGen = randint(1, 100)

    return ""

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    number = int(request.form['number'])
    sequence = ""
    if number <=0:
        return "Incorrect limit, please enter something higher than 0"
    n1 = 0
    n2 = 1
    result = 0
    sequence += str(n1)+", "
    sequence += str(n2)+", "
    while(n1+n2 <= number):
        result = n1+n2
        n1 = n2
        n2 = result
        sequence += str(result)+", "
        if(result == number):
            break
    return sequence